# PROJEKT SR 2014/15 - Rozproszony system transakcyjny #

Kacper Ostrowski

### Intro ###

Projekt jest udokumentowany w kodzie jak i w dodatkowej dokumentacji zamieszczonej
w root'cie repozytorium. Do projektu został wykorzystany przykładowy projekt z Visual Studio
(ścieżka: C:\Program Files (x86)\Microsoft WSE\v2.0\Samples\CS\QuickStart)
implementujący podstawową komunikacje w protokole SOAP. W ramach projektu na SR, 
ten przykładowy projekt został rozbudowany.

Pliki źródłowe:
~/AdminServer/AdminServer/Program.cs
~/Attachments/AttachmentsClient/AttachmentsClient.cs
~/Attachments/AttachmentsClient/CmdExecutor.cs
~/Attachments/AttachmentsService/servercode/*
~/Attachments/AttachmentsService/TransactionSystemServiceService.cs
~/Attachments/AttachmentsService/TransactionSystemServiceService.asmx