/*=====================================================================
  File:      AttachmentsClient.cs
 
  Summary:   This is a sample which allows the user to send a message 
             with an attachment.
 
---------------------------------------------------------------------
  This file is part of the Web Services Enhancements for Microsoft .NET Samples.

  Copyright (C) Microsoft Corporation.  All rights reserved.

This source code is intended only as a supplement to Microsoft
Development Tools and/or on-line documentation.  See these other
materials for detailed information regarding Microsoft code samples.

This sample is designed to demonstrate WSE features and is not intended 
for production use. Code and policy for a production application must be 
developed to meet the specific data and security requirements of the 
application.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
PARTICULAR PURPOSE.
=====================================================================*/

using System;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Text;

using Microsoft.Web.Services2;
using Microsoft.Web.Services2.Attachments;

using Microsoft.Web.Services2.QuickStart;

namespace Microsoft.Web.Services2.QuickStart.Attachments
{
    /// <summary>
    /// This is a sample which allows the user to send a message
    /// to a simple Web service that includes an attachment.
    /// </summary>
    public class AttachmentsClient : AppBase
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main(string[] args) 
        {
            try
            {
                string echoText = "This is the default text to be echoed back to the client by the service.";
                if(args != null && args.Length > 0)
                {
                    switch (args[0])
                    {
                        case "-?":
                        case "/?":
                        case "-help":
                        case "/help":
                            throw new InvalidArgumentException();
                        default:
                            echoText = args[0];
                            break;
                    }
                }

                AttachmentsClient client = new AttachmentsClient();
                client.Run(echoText);
            }
            catch (InvalidArgumentException)
            {
                Usage();
            }
            catch (Exception ex)
            {
                Error(ex);
                return;
            }

            Console.WriteLine( "" );
            Console.WriteLine( "Press [Enter] to continue..." );
            Console.ReadLine();
        }    
       
        static void Usage()
        {
            Console.WriteLine("Usage: Attachments string ");
            Console.WriteLine(" Required arguments:");
            Console.WriteLine("    string ".PadRight(20) + "A string. This will be echoed.");                
        }
    
        public void Run( string str )
        {
            //
            // Instantiate an instance of the Web service proxy
            //
            EchoAttachment serviceProxy = new EchoAttachment();

            //
            // Configure the proxy
            //
            ConfigureProxy(serviceProxy);

            //
            // create a memory stream for the string in the command line
            //
            Byte[] bytes = System.Text.Encoding.UTF8.GetBytes(str);
            MemoryStream buffer = new MemoryStream(bytes);
              
            //
            // Create a new Attachment class, and add the buffer to that
            //
            Attachment attachment = new Attachment("text/plain", buffer);
            serviceProxy.RequestSoapContext.Attachments.Add(attachment);

            //
            // Call the service
            //
            Console.WriteLine("Calling {0}", serviceProxy.Url);

            //
            // Success!                
            //
            Console.WriteLine("Web Service called successfully: {0}", serviceProxy.Echo()); 
        }

        class InvalidArgumentException : Exception
        {
            public InvalidArgumentException() : base()
            {
            }
        }
    }
}
 