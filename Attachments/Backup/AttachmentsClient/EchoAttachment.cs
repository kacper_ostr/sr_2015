﻿/*=====================================================================
  File:      EchoAttachments.cs
 
  Summary:   This is a sample which allows the user to send a message 
             with an attachment.
 
---------------------------------------------------------------------
  This file is part of the Web Services Enhancements for Microsoft .NET Samples.

  Copyright (C) Microsoft Corporation.  All rights reserved.

This source code is intended only as a supplement to Microsoft
Development Tools and/or on-line documentation.  See these other
materials for detailed information regarding Microsoft code samples.

This sample is designed to demonstrate WSE features and is not intended 
for production use. Code and policy for a production application must be 
developed to meet the specific data and security requirements of the 
application.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
PARTICULAR PURPOSE.
=====================================================================*/

using System.Diagnostics;
using System.Xml.Serialization;
using System;
using System.Web.Services.Protocols;
using System.ComponentModel;
using System.Web.Services;
using Microsoft.Web.Services2;

/// <remarks/>
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Web.Services.WebServiceBindingAttribute(Name="EchoAttachmentSoap", Namespace="http://microsoft.com/wse/samples/EchoAttachments")]
public class EchoAttachment : WebServicesClientProtocol {
    
    /// <remarks/>
    public EchoAttachment() {
        this.Url = "http://localhost/AttachmentsService/Attachments.asmx";
    }
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://microsoft.com/wse/samples/EchoAttachments/Echo", RequestNamespace="http://microsoft.com/wse/samples/EchoAttachments", ResponseNamespace="http://microsoft.com/wse/samples/EchoAttachments", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    public string Echo() {
        object[] results = this.Invoke("Echo", new object[0]);
        return ((string)(results[0]));
    }
    
    /// <remarks/>
    public System.IAsyncResult BeginEcho(System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("Echo", new object[0], callback, asyncState);
    }
    
    /// <remarks/>
    public string EndEcho(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((string)(results[0]));
    }
}
 