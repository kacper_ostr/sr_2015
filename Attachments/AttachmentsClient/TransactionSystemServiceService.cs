﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Ten kod został wygenerowany przez narzędzie.
//     Wersja wykonawcza:2.0.50727.8009
//
//     Zmiany w tym pliku mogą spowodować nieprawidłowe zachowanie i zostaną utracone, jeśli
//     kod zostanie ponownie wygenerowany.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

// 
// This source code was auto-generated by wsdl, Version=2.0.50727.3038.
// 
/*
 * kod wygenerowany z wspólnego wsdl-a
 */

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Web.Services.WebServiceBindingAttribute(Name="TransactionSystemServiceServiceSoap", Namespace="http://server")]
public partial class TransactionSystemServiceService : System.Web.Services.Protocols.SoapHttpClientProtocol {
    
    private System.Threading.SendOrPostCallback writeOperationCompleted;
    
    private System.Threading.SendOrPostCallback readOperationCompleted;
    
    private System.Threading.SendOrPostCallback refuseTransactionOperationCompleted;
    
    private System.Threading.SendOrPostCallback acceptTransactionOperationCompleted;
    
    private System.Threading.SendOrPostCallback lsOperationCompleted;
    
    private System.Threading.SendOrPostCallback writeBase64OperationCompleted;
    
    private System.Threading.SendOrPostCallback readBase64OperationCompleted;
    
    /// <remarks/>
    public TransactionSystemServiceService(string uri) {
        this.Url = uri;
    }

    public string getUrl()
    {
        return this.Url.ToString();
    }
    
    /// <remarks/>
    public event writeCompletedEventHandler writeCompleted;
    
    /// <remarks/>
    public event readCompletedEventHandler readCompleted;
    
    /// <remarks/>
    public event refuseTransactionCompletedEventHandler refuseTransactionCompleted;
    
    /// <remarks/>
    public event acceptTransactionCompletedEventHandler acceptTransactionCompleted;
    
    /// <remarks/>
    public event lsCompletedEventHandler lsCompleted;
    
    /// <remarks/>
    public event writeBase64CompletedEventHandler writeBase64Completed;
    
    /// <remarks/>
    public event readBase64CompletedEventHandler readBase64Completed;
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://server", ResponseNamespace="http://server", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    [return: System.Xml.Serialization.XmlElementAttribute("return", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public transactionResponse write([System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] string filepath) {
        object[] results = this.Invoke("write", new object[] {
                    filepath});
        return ((transactionResponse)(results[0]));
    }
    
    /// <remarks/>
    public System.IAsyncResult Beginwrite(string filepath, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("write", new object[] {
                    filepath}, callback, asyncState);
    }
    
    /// <remarks/>
    public transactionResponse Endwrite(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((transactionResponse)(results[0]));
    }
    
    /// <remarks/>
    public void writeAsync(string filepath) {
        this.writeAsync(filepath, null);
    }
    
    /// <remarks/>
    public void writeAsync(string filepath, object userState) {
        if ((this.writeOperationCompleted == null)) {
            this.writeOperationCompleted = new System.Threading.SendOrPostCallback(this.OnwriteOperationCompleted);
        }
        this.InvokeAsync("write", new object[] {
                    filepath}, this.writeOperationCompleted, userState);
    }
    
    private void OnwriteOperationCompleted(object arg) {
        if ((this.writeCompleted != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.writeCompleted(this, new writeCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://server", ResponseNamespace="http://server", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    [return: System.Xml.Serialization.XmlElementAttribute("return", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public response read([System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] string filepath) {
        object[] results = this.Invoke("read", new object[] {
                    filepath});
        return ((response)(results[0]));
    }
    
    /// <remarks/>
    public System.IAsyncResult Beginread(string filepath, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("read", new object[] {
                    filepath}, callback, asyncState);
    }
    
    /// <remarks/>
    public response Endread(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((response)(results[0]));
    }
    
    /// <remarks/>
    public void readAsync(string filepath) {
        this.readAsync(filepath, null);
    }
    
    /// <remarks/>
    public void readAsync(string filepath, object userState) {
        if ((this.readOperationCompleted == null)) {
            this.readOperationCompleted = new System.Threading.SendOrPostCallback(this.OnreadOperationCompleted);
        }
        this.InvokeAsync("read", new object[] {
                    filepath}, this.readOperationCompleted, userState);
    }
    
    private void OnreadOperationCompleted(object arg) {
        if ((this.readCompleted != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.readCompleted(this, new readCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://server", ResponseNamespace="http://server", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    [return: System.Xml.Serialization.XmlElementAttribute("return", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public response refuseTransaction([System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] long transactionId, [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] [System.Xml.Serialization.XmlIgnoreAttribute()] bool transactionIdSpecified) {
        object[] results = this.Invoke("refuseTransaction", new object[] {
                    transactionId,
                    transactionIdSpecified});
        return ((response)(results[0]));
    }
    
    /// <remarks/>
    public System.IAsyncResult BeginrefuseTransaction(long transactionId, bool transactionIdSpecified, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("refuseTransaction", new object[] {
                    transactionId,
                    transactionIdSpecified}, callback, asyncState);
    }
    
    /// <remarks/>
    public response EndrefuseTransaction(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((response)(results[0]));
    }
    
    /// <remarks/>
    public void refuseTransactionAsync(long transactionId, bool transactionIdSpecified) {
        this.refuseTransactionAsync(transactionId, transactionIdSpecified, null);
    }
    
    /// <remarks/>
    public void refuseTransactionAsync(long transactionId, bool transactionIdSpecified, object userState) {
        if ((this.refuseTransactionOperationCompleted == null)) {
            this.refuseTransactionOperationCompleted = new System.Threading.SendOrPostCallback(this.OnrefuseTransactionOperationCompleted);
        }
        this.InvokeAsync("refuseTransaction", new object[] {
                    transactionId,
                    transactionIdSpecified}, this.refuseTransactionOperationCompleted, userState);
    }
    
    private void OnrefuseTransactionOperationCompleted(object arg) {
        if ((this.refuseTransactionCompleted != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.refuseTransactionCompleted(this, new refuseTransactionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://server", ResponseNamespace="http://server", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    [return: System.Xml.Serialization.XmlElementAttribute("return", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public response acceptTransaction([System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] long transactionId, [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] [System.Xml.Serialization.XmlIgnoreAttribute()] bool transactionIdSpecified) {
        object[] results = this.Invoke("acceptTransaction", new object[] {
                    transactionId,
                    transactionIdSpecified});
        return ((response)(results[0]));
    }
    
    /// <remarks/>
    public System.IAsyncResult BeginacceptTransaction(long transactionId, bool transactionIdSpecified, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("acceptTransaction", new object[] {
                    transactionId,
                    transactionIdSpecified}, callback, asyncState);
    }
    
    /// <remarks/>
    public response EndacceptTransaction(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((response)(results[0]));
    }
    
    /// <remarks/>
    public void acceptTransactionAsync(long transactionId, bool transactionIdSpecified) {
        this.acceptTransactionAsync(transactionId, transactionIdSpecified, null);
    }
    
    /// <remarks/>
    public void acceptTransactionAsync(long transactionId, bool transactionIdSpecified, object userState) {
        if ((this.acceptTransactionOperationCompleted == null)) {
            this.acceptTransactionOperationCompleted = new System.Threading.SendOrPostCallback(this.OnacceptTransactionOperationCompleted);
        }
        this.InvokeAsync("acceptTransaction", new object[] {
                    transactionId,
                    transactionIdSpecified}, this.acceptTransactionOperationCompleted, userState);
    }
    
    private void OnacceptTransactionOperationCompleted(object arg) {
        if ((this.acceptTransactionCompleted != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.acceptTransactionCompleted(this, new acceptTransactionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://server", ResponseNamespace="http://server", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    [return: System.Xml.Serialization.XmlElementAttribute("return", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string[] ls() {
        object[] results = this.Invoke("ls", new object[0]);
        return ((string[])(results[0]));
    }
    
    /// <remarks/>
    public System.IAsyncResult Beginls(System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("ls", new object[0], callback, asyncState);
    }
    
    /// <remarks/>
    public string[] Endls(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((string[])(results[0]));
    }
    
    /// <remarks/>
    public void lsAsync() {
        this.lsAsync(null);
    }
    
    /// <remarks/>
    public void lsAsync(object userState) {
        if ((this.lsOperationCompleted == null)) {
            this.lsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnlsOperationCompleted);
        }
        this.InvokeAsync("ls", new object[0], this.lsOperationCompleted, userState);
    }
    
    private void OnlsOperationCompleted(object arg) {
        if ((this.lsCompleted != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.lsCompleted(this, new lsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://server", ResponseNamespace="http://server", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    [return: System.Xml.Serialization.XmlElementAttribute("return", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public transactionResponse writeBase64(string filepath, string content) {
        object[] results = this.Invoke("writeBase64", new object[] {
                    filepath,
                    content});
        return ((transactionResponse)(results[0]));
    }
    
    /// <remarks/>
    public System.IAsyncResult BeginwriteBase64(string filepath, string content, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("writeBase64", new object[] {
                    filepath,
                    content}, callback, asyncState);
    }
    
    /// <remarks/>
    public transactionResponse EndwriteBase64(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((transactionResponse)(results[0]));
    }
    
    /// <remarks/>
    public void writeBase64Async(string filepath, string content) {
        this.writeBase64Async(filepath, content, null);
    }
    
    /// <remarks/>
    public void writeBase64Async(string filepath, string content, object userState) {
        if ((this.writeBase64OperationCompleted == null)) {
            this.writeBase64OperationCompleted = new System.Threading.SendOrPostCallback(this.OnwriteBase64OperationCompleted);
        }
        this.InvokeAsync("writeBase64", new object[] {
                    filepath,
                    content}, this.writeBase64OperationCompleted, userState);
    }
    
    private void OnwriteBase64OperationCompleted(object arg) {
        if ((this.writeBase64Completed != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.writeBase64Completed(this, new writeBase64CompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://server", ResponseNamespace="http://server", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    [return: System.Xml.Serialization.XmlElementAttribute("return", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string readBase64(string filepath) {
        object[] results = this.Invoke("readBase64", new object[] {
                    filepath});
        return ((string)(results[0]));
    }
    
    /// <remarks/>
    public System.IAsyncResult BeginreadBase64(string filepath, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("readBase64", new object[] {
                    filepath}, callback, asyncState);
    }
    
    /// <remarks/>
    public string EndreadBase64(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((string)(results[0]));
    }
    
    /// <remarks/>
    public void readBase64Async(string filepath) {
        this.readBase64Async(filepath, null);
    }
    
    /// <remarks/>
    public void readBase64Async(string filepath, object userState) {
        if ((this.readBase64OperationCompleted == null)) {
            this.readBase64OperationCompleted = new System.Threading.SendOrPostCallback(this.OnreadBase64OperationCompleted);
        }
        this.InvokeAsync("readBase64", new object[] {
                    filepath}, this.readBase64OperationCompleted, userState);
    }
    
    private void OnreadBase64OperationCompleted(object arg) {
        if ((this.readBase64Completed != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.readBase64Completed(this, new readBase64CompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    public new void CancelAsync(object userState) {
        base.CancelAsync(userState);
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://server")]
public partial class transactionResponse : response {
    
    private long transactionIdField;
    
    private bool transactionIdFieldSpecified;
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public long transactionId {
        get {
            return this.transactionIdField;
        }
        set {
            this.transactionIdField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool transactionIdSpecified {
        get {
            return this.transactionIdFieldSpecified;
        }
        set {
            this.transactionIdFieldSpecified = value;
        }
    }
}

/// <uwagi/>
[System.Xml.Serialization.XmlIncludeAttribute(typeof(transactionResponse))]
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://server")]
public partial class response {
    
    private string messageField;
    
    private responseType typeField;
    
    private bool typeFieldSpecified;
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string message {
        get {
            return this.messageField;
        }
        set {
            this.messageField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public responseType type {
        get {
            return this.typeField;
        }
        set {
            this.typeField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool typeSpecified {
        get {
            return this.typeFieldSpecified;
        }
        set {
            this.typeFieldSpecified = value;
        }
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://server")]
public enum responseType {
    
    /// <uwagi/>
    SUCCESS,
    
    /// <uwagi/>
    FAILURE,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void writeCompletedEventHandler(object sender, writeCompletedEventArgs e);

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public partial class writeCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
    
    private object[] results;
    
    internal writeCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
            base(exception, cancelled, userState) {
        this.results = results;
    }
    
    /// <remarks/>
    public transactionResponse Result {
        get {
            this.RaiseExceptionIfNecessary();
            return ((transactionResponse)(this.results[0]));
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void readCompletedEventHandler(object sender, readCompletedEventArgs e);

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public partial class readCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
    
    private object[] results;
    
    internal readCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
            base(exception, cancelled, userState) {
        this.results = results;
    }
    
    /// <remarks/>
    public response Result {
        get {
            this.RaiseExceptionIfNecessary();
            return ((response)(this.results[0]));
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void refuseTransactionCompletedEventHandler(object sender, refuseTransactionCompletedEventArgs e);

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public partial class refuseTransactionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
    
    private object[] results;
    
    internal refuseTransactionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
            base(exception, cancelled, userState) {
        this.results = results;
    }
    
    /// <remarks/>
    public response Result {
        get {
            this.RaiseExceptionIfNecessary();
            return ((response)(this.results[0]));
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void acceptTransactionCompletedEventHandler(object sender, acceptTransactionCompletedEventArgs e);

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public partial class acceptTransactionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
    
    private object[] results;
    
    internal acceptTransactionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
            base(exception, cancelled, userState) {
        this.results = results;
    }
    
    /// <remarks/>
    public response Result {
        get {
            this.RaiseExceptionIfNecessary();
            return ((response)(this.results[0]));
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void lsCompletedEventHandler(object sender, lsCompletedEventArgs e);

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public partial class lsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
    
    private object[] results;
    
    internal lsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
            base(exception, cancelled, userState) {
        this.results = results;
    }
    
    /// <remarks/>
    public string[] Result {
        get {
            this.RaiseExceptionIfNecessary();
            return ((string[])(this.results[0]));
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void writeBase64CompletedEventHandler(object sender, writeBase64CompletedEventArgs e);

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public partial class writeBase64CompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
    
    private object[] results;
    
    internal writeBase64CompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
            base(exception, cancelled, userState) {
        this.results = results;
    }
    
    /// <remarks/>
    public transactionResponse Result {
        get {
            this.RaiseExceptionIfNecessary();
            return ((transactionResponse)(this.results[0]));
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void readBase64CompletedEventHandler(object sender, readBase64CompletedEventArgs e);

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public partial class readBase64CompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
    
    private object[] results;
    
    internal readBase64CompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
            base(exception, cancelled, userState) {
        this.results = results;
    }
    
    /// <remarks/>
    public string Result {
        get {
            this.RaiseExceptionIfNecessary();
            return ((string)(this.results[0]));
        }
    }
}
