/*=====================================================================
  File:      AttachmentsClient.cs
 
  Summary:   This is a sample which allows the user to send a message 
             with an attachment.
 
---------------------------------------------------------------------
  This file is part of the Web Services Enhancements for Microsoft .NET Samples.

  Copyright (C) Microsoft Corporation.  All rights reserved.

This source code is intended only as a supplement to Microsoft
Development Tools and/or on-line documentation.  See these other
materials for detailed information regarding Microsoft code samples.

This sample is designed to demonstrate WSE features and is not intended 
for production use. Code and policy for a production application must be 
developed to meet the specific data and security requirements of the 
application.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
PARTICULAR PURPOSE.
=====================================================================*/

using System;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Text;

using Microsoft.Web.Services2;
using Microsoft.Web.Services2.Attachments;

using Microsoft.Web.Services2.QuickStart;

using Attachments;

namespace Microsoft.Web.Services2.QuickStart.Attachments
{
    /// <summary>
    /// This is a sample which allows the user to send a message
    /// to a simple Web service that includes an attachment.
    /// </summary>
    public class AttachmentsClient : AppBase
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main(string[] args) 
        {
            bool runClient = true;
            CmdExecutor cmdExec = new CmdExecutor();

            Console.WriteLine("Witam w aplikacji klienckiej. W celu otrzymania pomocy wpisz help.\n");

            while (runClient)
            {
                Console.Write("> ");
                string cmd = Console.ReadLine();
                if (cmd.Trim() == "q")
                    Environment.Exit(0);
                cmdExec.executeCmd(cmd);
            }
        }          
    }
}
 