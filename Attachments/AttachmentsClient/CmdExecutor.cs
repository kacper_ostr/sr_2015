﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Attachments
{
    class CmdExecutor
    {
        private const string ls = "ls";
        private const string write = "write";
        private const string read = "read";
        private const string help = "help";
        private const string addServer = "addserver";
        private const string rmServer = "rmserver";
        private const string listServer = "listserver";
        private const string addMultipleFiles = "addmultiplefiles";
        private Dictionary<string, TransactionSystemServiceService> serviceList = new Dictionary<string, TransactionSystemServiceService>();

        public void executeCmd(string cmd)
        {
            cmd = cmd.Trim();
            string[] args = cmd.Split();

            if (serviceList.Count == 0 && args[0] != addServer && args[0] != help)
            {
                Console.WriteLine("Nie podano żadnego serwisu do obsługi.");
                return;
            }

            switch (args[0])
            {
                case ls : getFileList(); break;
                case write : writeFile(args[1], args[2]); break;
                case read: readFile(args[1], args[2]); break;
                case help: displayHelp();  break;
                case addServer : addServerCmd(args[1], args[2]); break;
                case rmServer: rmServerCmd(args[1]); break;
                case listServer: listServers(); break;
                case addMultipleFiles: addMultipleFilesCmd(args); break;
                default:
                    Console.WriteLine("Nieprawidlowa komenda. Wpisz 'help', aby otrzymac liste komend.");
                    break;
            }
        }

        private void getFileList()
        {
            foreach (string tmp in serviceList.Keys)
            {
                try
                {
                    string[] filelist = serviceList[tmp].ls();
                    Console.WriteLine(" ROOT: " + tmp);
                    Console.WriteLine(" |");
                    for (int i = 0; i < filelist.Length; ++i)
                    {
                        Console.WriteLine(" -" + filelist[i]);
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine("Wystapil blad:\n" + e.Message);
                }
            }
        }

        private bool writeFile(string fileName, string serviceName)
        {
            try
            {
                TransactionSystemServiceService service = serviceList[serviceName];
                Byte[] fileBytes = File.ReadAllBytes(fileName);
                string content = Convert.ToBase64String(fileBytes);
                transactionResponse tr = service.writeBase64(parseFilePath(fileName), content);

                if (tr.transactionId != -1)
                {
                    response accResp = service.acceptTransaction(tr.transactionId, true);        
                    Console.WriteLine("Transakcja zakonczona powodzenieniem");
                    return true;
                }
                else
                {
                    Console.WriteLine("Transakcja zakonczona niepowodzeniem");
                    return false;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Wystapil blad:\n" + e.Message);
                Console.WriteLine("Transakcja zakonczona niepowodzeniem");
                return false;
            }
        }

        private void readFile(string filename, string serviceName)
        {
            try
            {
                TransactionSystemServiceService service = serviceList[serviceName];
                string base64String = service.readBase64(filename);
                byte[] fileContent = Convert.FromBase64String(base64String);
                File.WriteAllBytes(filename, fileContent);
                Console.WriteLine("Plik odebrany");
            }
            catch (Exception e)
            {
                Console.WriteLine("Wystapil blad:\n" + e.Message);
            }
        }

        private void displayHelp()
        {
            Console.WriteLine("ls - wyswietl liste plikow");
            Console.WriteLine("write <filename> <servername> - zapisz plik na serwer");
            Console.WriteLine("read <filename> <servername> - odczytaj plik z serwera");
            Console.WriteLine("addserver <severname> <serveraddress> - dodaj serwer o podanej nazwie i url-u");
            Console.WriteLine("rmserver <severname> - usun serwer o podanej nazwie");
            Console.WriteLine("listserver - wyswietl liste serwerow");
            Console.WriteLine("addmultiplefiles <filename1> <servername1> <filename2> <servername2> ... <filenameN> <servernameN> - zapisz podane pliki na konkretne serwery");
            Console.WriteLine("q - wyjdz"); 
        }

        private void addServerCmd(string name, string uri)
        {
            try
            {
                serviceList.Add(name, new TransactionSystemServiceService(uri));
            }
            catch(Exception e)
            {
                Console.WriteLine("Wystapil blad: \n" + e.Message);
            }
        }

        private void listServers()
        {
            foreach (string tmp in serviceList.Keys)
            {
                Console.WriteLine(" - " + tmp + " url: " + serviceList[tmp].getUrl());
            }
        }

        private void rmServerCmd(string name)
        {
            try
            {
                serviceList.Remove(name);
            }
            catch (Exception e)
            {
                Console.WriteLine("Wystapil blad: \n" + e.Message);
            }
        }

        private void addMultipleFilesCmd(string[] filelist)
        {
            try
            {                
                if((filelist.Length-1)%2!=0)
                {
                    Console.WriteLine("Nieprawidlowa lista argumentow!");
                    return;
                }

                Dictionary<string, List<transactionResponse>> transactionList = new Dictionary<string, List<transactionResponse>>();    

                for (int i = 1; i < filelist.Length - 1; i+=2 )
                {                   
                   TransactionSystemServiceService service = serviceList[filelist[i + 1]];
                   Byte[] fileBytes = File.ReadAllBytes(filelist[i]);
                   string content = Convert.ToBase64String(fileBytes);
                   transactionResponse tr = service.writeBase64(parseFilePath(filelist[i]), content);
                   if (!tr.transactionIdSpecified || tr.transactionId == -1)
                   {
                       //ROLLBACK - transakcja w jednym z serwerow nie powiodla sie
                       foreach (string serviceName in transactionList.Keys)
                       {
                           TransactionSystemServiceService tmpservice = serviceList[serviceName];
                           foreach (transactionResponse tmpTr in transactionList[serviceName])
                           tmpservice.refuseTransaction( tmpTr.transactionId, false );
                       }
                       Console.WriteLine("Transakcja zakonczona niepowodzeniem");
                       return;
                   }
                   //transactionList.Add(filelist[i], tr);
                   try
                   {
                       transactionList[filelist[i+1]].Add(tr);
                   }
                    catch(Exception e)
                   {
                        //COMMIT - transakcje wszedzie sie odbyly
                       transactionList.Add(filelist[i+1], new List<transactionResponse>());
                       transactionList[filelist[i+1]].Add(tr);
                   }
                }
                foreach (string serviceName in transactionList.Keys)
                {
                    TransactionSystemServiceService tmpservice = serviceList[serviceName];
                    foreach (transactionResponse tmpTr in transactionList[serviceName])
                        tmpservice.acceptTransaction(tmpTr.transactionId, true);
                }
                Console.WriteLine("Transakcja zakonczona powodzeniem");
            }
            catch (Exception e)
            {
                Console.WriteLine("Wystapil blad: \n" + e.Message);
            }
        }

        private string parseFilePath(string filepath) 
        {
            string[] f = filepath.Split(new Char[] { '/', '\\' });
            string filename = f[f.Length - 1];
            return filename;
        }
    }
}
