﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Ten kod został wygenerowany przez narzędzie.
//     Wersja wykonawcza:2.0.50727.8009
//
//     Zmiany w tym pliku mogą spowodować nieprawidłowe zachowanie i zostaną utracone, jeśli
//     kod zostanie ponownie wygenerowany.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using System.IO;
using AttachmentsService.servercode;

// 
// This source code was auto-generated by wsdl, Version=2.0.50727.3038.
// 


/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.Web.Services.WebServiceAttribute(Namespace="http://server")]
[System.Web.Services.WebServiceBindingAttribute(Name="TransactionSystemServicePortBinding", Namespace="http://server")]
[SoapDocumentService(RoutingStyle = SoapServiceRoutingStyle.RequestElement)]
public  class TransactionSystemServiceService : System.Web.Services.WebService {

    private FileSystemExplorer fileSystemExplorer = new FileSystemExplorer();
    private TransactionManager transactionManager = Global.transactionManager;

    /// <remarks/>
    [System.Web.Services.WebMethodAttribute()]
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://server", ResponseNamespace="http://server", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    [return: System.Xml.Serialization.XmlElementAttribute("return", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public transactionResponse write([System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)] string filepath)
    {
        return new transactionResponse();
    }
    
    /// <remarks/>
    [System.Web.Services.WebMethodAttribute()]
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://server", ResponseNamespace="http://server", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    [return: System.Xml.Serialization.XmlElementAttribute("return", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public  response read([System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] string filepath)
    {
        return new response();
    }
    
    /// <remarks/>
    [System.Web.Services.WebMethodAttribute()]
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://server", ResponseNamespace="http://server", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    [return: System.Xml.Serialization.XmlElementAttribute("return", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public  response refuseTransaction([System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] long transactionId, [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] [System.Xml.Serialization.XmlIgnoreAttribute()] bool transactionIdSpecified)
    {
        response r = new response();
        if (Global.mode
                    || (!Global.mode &&
                            Global.admin.askAdmin("Czy potwierdzic odrzucenie transakcji (w przeciwnym razie zostanie zaakceptowana): " + transactionManager.getTransactionInfo(transactionId))))
        {
            transactionManager.rollbackTransaction(transactionId);
            r.typeSpecified = false;
        }
        else
        {
            transactionManager.commitTransaction(transactionId);
            r.typeSpecified = true;
        }
        return r;
    }
    
    /// <remarks/>
    [System.Web.Services.WebMethodAttribute()]
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://server", ResponseNamespace="http://server", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    [return: System.Xml.Serialization.XmlElementAttribute("return", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public  response acceptTransaction([System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] long transactionId, [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)] [System.Xml.Serialization.XmlIgnoreAttribute()] bool transactionIdSpecified)
    {
        response r = new response();
        if (Global.mode
                    ||
                    (!Global.mode &&
                           Global.admin.askAdmin("Czy potwierdizc powodzenie transakcji: " + transactionManager.getTransactionInfo(transactionId))))
        {
            transactionManager.commitTransaction(transactionId);
            r.typeSpecified = true;
        }
        else
        {
            transactionManager.rollbackTransaction(transactionId);
            r.typeSpecified = false;
        }
        return new response();
    }
    
    /// <remarks/>
    [System.Web.Services.WebMethodAttribute()]
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://server", ResponseNamespace="http://server", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    [return: System.Xml.Serialization.XmlElementAttribute("return", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public  string[] ls()
    {
        return fileSystemExplorer.getFileList();
    }

    /// <remarks/>
    [System.Web.Services.WebMethodAttribute()]
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace = "http://server", ResponseNamespace = "http://server", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    [return: System.Xml.Serialization.XmlElementAttribute("return", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public transactionResponse writeBase64(string filepath, string content)
    {
        byte[] fileContent = Convert.FromBase64String(content);
        long result = transactionManager.executeWriteTransaction(filepath, fileContent);
        transactionResponse tr = new transactionResponse();
        tr.transactionId = result;
        if (result != -1)            
            tr.transactionIdSpecified = true;
        else        
            tr.transactionIdSpecified = false;            

        return tr;
    }

    /// <remarks/>
    [System.Web.Services.WebMethodAttribute()]
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace = "http://server", ResponseNamespace = "http://server", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    [return: System.Xml.Serialization.XmlElementAttribute("return", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string readBase64(string filepath)
    {
        string resp = transactionManager.executeReadTransaction(filepath);        
        return resp;
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://server")]
public partial class transactionResponse : response {
    
    private long transactionIdField;
    
    private bool transactionIdFieldSpecified;
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public long transactionId {
        get {
            return this.transactionIdField;
        }
        set {
            this.transactionIdField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool transactionIdSpecified {
        get {
            return this.transactionIdFieldSpecified;
        }
        set {
            this.transactionIdFieldSpecified = value;
        }
    }
}
/// <uwagi/>
/// 
[System.Xml.Serialization.XmlIncludeAttribute(typeof(transactionResponse))]
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://server")]
public partial class response {
    
    private string messageField;
    
    private responseType typeField;
    
    private bool typeFieldSpecified;
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string message {
        get {
            return this.messageField;
        }
        set {
            this.messageField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public responseType type {
        get {
            return this.typeField;
        }
        set {
            this.typeField = value;
        }
    }
    
    /// <uwagi/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool typeSpecified {
        get {
            return this.typeFieldSpecified;
        }
        set {
            this.typeFieldSpecified = value;
        }
    }
}

/// <uwagi/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://server")]
public enum responseType {
    
    /// <uwagi/>
    SUCCESS,
    
    /// <uwagi/>
    FAILURE,
}
