/*=====================================================================
  File:      Echo.asmx.cs
 
  Summary:   Illustrates the code behind simple Web service which
             puts an expiration timestamp on responses.
 
---------------------------------------------------------------------
  This file is part of the Web Services Enhancements for Microsoft .NET Samples.

  Copyright (C) Microsoft Corporation.  All rights reserved.

This source code is intended only as a supplement to Microsoft
Development Tools and/or on-line documentation.  See these other
materials for detailed information regarding Microsoft code samples.

This sample is designed to demonstrate WSE features and is not intended 
for production use. Code and policy for a production application must be 
developed to meet the specific data and security requirements of the 
application.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
PARTICULAR PURPOSE.
=====================================================================*/

using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Microsoft.Web.Services2;
using Microsoft.Web.Services2.Dime;
using System.IO;

namespace Microsoft.Web.Services2.QuickStart.Attachments
{    
    [WebService(Namespace="http://microsoft.com/wse/samples/EchoAttachments")]
    public class EchoAttachment : System.Web.Services.WebService
    {
        [WebMethod]
        public string Echo()
        {
            // Reject any requests which are not valid SOAP requests
            if (RequestSoapContext.Current == null)
                throw new ApplicationException("Only SOAP requests are permitted.");

            if (RequestSoapContext.Current.Attachments.Count == 0)
                throw new ApplicationException("No attachments were sent with the message.");

            //
            // Read the attachment into a string
            // We'll do this by creating a StreamReader which we'll use to read
            // the stream for the first 
            //
            String str = "";
            StreamReader stream = new StreamReader(RequestSoapContext.Current.Attachments[0].Stream);
            str = stream.ReadToEnd();
            stream.Close();

            //
            // Return the string
            //
            return str;
        }
    }
}
 