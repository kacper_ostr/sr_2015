﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttachmentsService.servercode
{
    /*
     * klasa konfigurująca wszystkie ścieżki katalogów
     */
    public static class Configure
    {
        public static string root = "D:/Pulpit/SR_PROJEKT/Attachments/AttachmentsService/";
        public static string filesystemPath = root + "filesystem/";
        public static string seqPath = root + "seq/seq_file";
        public static string logPath = root + "log/";        
    }
}