﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Timers;

namespace AttachmentsService.servercode
{
    /*
     * klasa reprezentujaca pojedynczą transakcje i operacje, które można na niej wykonać
     */
    public class Transaction
    {
        //id transakcji
        private long transactionId;
        //referencja do mutexa blokujacego transakcje
        private Mutex fileLockRef;
        //plik którego dotyczy transakcja
        private string filePath;
        //licznik odliczający czas do timeout'a w przypadku gdy podczas operacji na pliku coś pójdzie nie tak. po jego minieciu transakcja jest anulowana
        private System.Timers.Timer preCommitTimeout = new System.Timers.Timer(10000);
        //licznik odliczający czas do timeout'a jesli nie nadejdzie commit od klienta. po jego minieciu transakcja jest anulowana
        private System.Timers.Timer commitTimeout = new System.Timers.Timer(30000);
        //stan w jakim jest transakcja
        private string state;
        //referencja do obsługi plików
        private FileSystemExplorer fileExplorer;
        //typ transakcji
        private transactionType tType;

        public Transaction(string file, Mutex fileMutex, transactionType type)
        {
            transactionId = TransactionSequence.getNextID();
            fileLockRef = fileMutex;
            filePath = file;
            preCommitTimeout.Elapsed += OnPreCommitTimeOut;
            preCommitTimeout.Enabled = true;
            state = "BEGIN";
            fileExplorer = new FileSystemExplorer();
            tType = type;
        }
        
        private void OnPreCommitTimeOut(Object source, ElapsedEventArgs e)
        {
            try
            {
                Monitor.Exit(fileLockRef);
            }
            catch(Exception er)
            {

            }
            preCommitTimeout.Enabled = false;
        }

        private void OnCommitTimeOut(Object source, ElapsedEventArgs e)
        {
            commitTimeout.Enabled = false;
            fileExplorer.deleteFile(filePath+"_TMP");
            state = "FAILED";
        }

        //wywoływane jeśli operacja na pliku poszła ok i potrzebny jest sygnał od klienta, że można zaakceptować zmiany
        public void endTransaction()
        {
            preCommitTimeout.Enabled = false;
            state = "PRECOMITTED";
            commitTimeout.Enabled = true;
            commitTimeout.Elapsed += OnCommitTimeOut;           
        }

        public long getTransactionId()
        {
            return transactionId;
        }

        public void commitTransaction()
        {
            try
            {
                Monitor.Exit(fileLockRef);
            }
            catch (Exception e)
            { }
            commitTimeout.Enabled = false;
            state = "COMITTED";
        }

        public void rollbackTransaction()
        {
            try
            {
                Monitor.Exit(fileLockRef);
            }
            catch (Exception e)
            { }
            commitTimeout.Enabled = false;
            fileExplorer.deleteFile(filePath + "_TMP");
            state = "ROLLBACK";
        }

        public string getFileName()
        {
            return filePath;
        }

        public string info()
        {
            string msg;
                if (tType == transactionType.READ)
                    msg = "odczyt";
                else
                    msg = "zapis";
            return "ID: " + transactionId + " , plik: " + filePath + " typ: " + msg + " stan " + state ;
        }
    }           
}