﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;

namespace AttachmentsService.servercode
{
    /*
     * klasa zarządzająca transakcjami. decyduje czy można wykonać transakcje na danym pliku
     */
    public class TransactionManager
    {
        //lista plików i związanych z nimi mutexów
        Dictionary<string, Locker> fileList;
        //lista transakcji na plikach
        Dictionary<long, Transaction> transactionList;
        //referencja do menadżera plików
        FileSystemExplorer fileExplorer;

        public TransactionManager()
        {
            fileList = new Dictionary<string, Locker>();
            fileExplorer = new FileSystemExplorer();
            string[] files = fileExplorer.getFileList();
            foreach (string file in files)
            {
                fileList.Add(file, new Locker());
            }
            transactionList = new Dictionary<long, Transaction>();
        }

        //funkcja określająca czy można na danycm pliku wykonać operacje - dopuszcza wielokrtony odczyt (pod warunkiem, ze nie ma zapisu na pliku)
        //i pojedynczy zapis (odmawia odczytu lub innego zapisu na tym samym pliku)
        private bool canBeginTransaction(string filename, transactionType type)
        {
            //automatyczna czy manualna decyzja
            if (Global.mode)
            {
                Locker tmpLocker = null;

                if (checkIfFileExists(filename))
                    tmpLocker = fileList[filename];

                if (tmpLocker == null)
                {
                    fileList.Add(filename, new Locker());
                    tmpLocker = fileList[filename];
                    tmpLocker.tType = type;
                    return Monitor.TryEnter(tmpLocker.locker);
                }
                else if (Monitor.TryEnter(tmpLocker.locker))
                {
                    tmpLocker.tType = type;
                    return true;
                }
                else if (!Monitor.TryEnter(tmpLocker.locker) && tmpLocker.tType == transactionType.READ && type == transactionType.READ)
                {
                    transactionList.Last().Value.commitTransaction();
                    return true;
                }
                else
                    return false;
            }
            else
           {
                string msg;
                if (type == transactionType.READ)
                    msg = "odczytu";
                else
                    msg = "zapisu";

                if (Global.admin.askAdmin("Czy akceptujesz próbę " + msg + " pliku " + filename))
                {
                    if (!checkIfFileExists(filename)) 
                    {
                        fileList.Add(filename, new Locker());
                       Locker tmpLocker = null;
                        tmpLocker = fileList[filename];
                       tmpLocker.tType = type;
                    }

                    return true;
                }
                return false;
            }
        }

        //funkcja wykonujaca transakcje zapisywania. wynikiem jest stan PRECOMITTED i oczekiwnie na commit od klienta
        public long executeWriteTransaction(string filename, byte[] content)
        {
            if (canBeginTransaction(filename, transactionType.WRITE))
            {
                Transaction newTransaction = new Transaction(filename, fileList[filename].locker, transactionType.WRITE);
                transactionList.Add(newTransaction.getTransactionId(), newTransaction);
                fileExplorer.writeToFile(filename + "_TMP", content);
                newTransaction.endTransaction();
                return newTransaction.getTransactionId();
            }
            else
                return -1;
        }

        //funkcja wykonujaca odczyt. zwraca zawartosc pliku
        public string executeReadTransaction(string filename)
        {
            if (canBeginTransaction(filename, transactionType.READ))
            {
                Transaction newTransaction = new Transaction(filename, fileList[filename].locker, transactionType.READ);
                transactionList.Add(newTransaction.getTransactionId(), newTransaction);
                string base64Resp = Convert.ToBase64String(fileExplorer.readBytesFromFile(filename));
                newTransaction.endTransaction();
                return base64Resp;
            }
            else
                return "ERROR";            
        }

        //commit transakcji przez klienta. PRECOMITTED -> COMITTED
        public void commitTransaction(long id)
        {
            Transaction transaction = transactionList[id];            
            string oldName = transaction.getFileName() + "_TMP";
            string newName = transaction.getFileName();
            fileExplorer.renameFile(oldName, newName);
            transaction.commitTransaction();
        }        

        //wycofanie transakcji przez klienta PRECOMITTED -> ROLLBACK
        public void rollbackTransaction(long id)
        {            
            Transaction transaction = transactionList[id];
            transaction.rollbackTransaction();
            string oldName = transaction.getFileName() + "_TMP";
            fileExplorer.deleteFile(oldName);
        }

        //funkcja sprawdzająca czy żadany plik istnieje
        private bool checkIfFileExists(string filename)
        {
            try
            {
                Locker tmp = fileList[filename];
            } 
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        //funkcja podająca dane transakcji o zadanym id
        public string getTransactionInfo(long id)
        {
            try
            {
                return transactionList[id].info();
            }
            catch(Exception e)
            {
                return "blad";
            }
        }
    }
    public enum transactionType
    {
        /// <uwagi/>
        READ,

        /// <uwagi/>
        WRITE,

        NONE,
    }
}