﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttachmentsService.servercode
{
    /*
     * zbiór obiektów często wykorzystywanych i takich które muszą przechowywać stan aplikacji
     */
    public static class Global
    {
        public static TransactionManager transactionManager = new TransactionManager();
        public static AdminConnector admin = new AdminConnector();
        //true - automat, false - manual
        public static bool mode = false;
    }
}