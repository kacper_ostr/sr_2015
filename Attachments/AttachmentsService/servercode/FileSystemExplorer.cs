﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace AttachmentsService.servercode
{
    /*
     * klasa odpowiedzialna za operacje na plikach w lokalnym filesystemie. założenie jest, że filesystem jest jednopoziomowy
     * metody sa jednoznacznie opisane, więc dalszy komentarz jest niepotrzebny
     */
    public class FileSystemExplorer
    {

        private string fileSystemRoot;

        public FileSystemExplorer()
        {
            fileSystemRoot = Configure.filesystemPath;
        }        

        public string[] getFileList()
        {
            string[] filePathList = Directory.GetFiles(fileSystemRoot);
            List<string> fileList = new List<string>();
            for (int i = 0; i < filePathList.Length; i++)
            {
                //pominięcie plików tmp, nie powinny być widoczne dla użytkownika
                if (filePathList[i].Substring(filePathList[i].Length - 4) != "_TMP")
                {
                    string[] tmp = filePathList[i].Split(new Char[] { '/' });
                    fileList.Add(tmp[tmp.Length - 1]);
                }                
            }            
            return fileList.ToArray<string>();
        }

        public void writeToFile(string filepath, string content)
        {
            string absolutePath = fileSystemRoot + filepath;
            File.WriteAllText(absolutePath, content);
        }

        public void writeToFile(string filepath, byte[] content)
        {
            string absolutePath = fileSystemRoot + filepath;
            File.WriteAllBytes(absolutePath, content);
        }

        public void deleteFile(string path)
        {
            string absolutePath = fileSystemRoot + path;
            if (File.Exists(absolutePath))
            {
                File.Delete(absolutePath);
            }
        }

        public void renameFile(string oldName, string newName)
        {
            string src = fileSystemRoot + oldName;
            string dst = fileSystemRoot + newName;

            if (File.Exists(dst))
                File.Delete(dst);

            File.Copy(src, dst);
            File.Delete(src);
        }

        public string readStringFromFile(string filename)
        {            
            string content = File.ReadAllText(fileSystemRoot + filename);
            return content;
        }

        public byte[] readBytesFromFile(string filename)
        {
            byte[] content = File.ReadAllBytes(fileSystemRoot + filename);
            return content;
        }
    }
}