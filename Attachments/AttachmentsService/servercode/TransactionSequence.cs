﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace AttachmentsService.servercode
{
    /*
     * klasa wzorowana na sekwencji w bazach danych
     */
    public static class TransactionSequence
    {        
        //pobiera id z pliku, inkrementuje i zwraca. gwarancja, ze sie nie powtorzy
        public static long getNextID()
        {
            string newId = File.ReadAllText(Configure.seqPath);
            long idToReturn = Convert.ToInt64(newId);
            ++idToReturn;
            File.WriteAllText(Configure.seqPath, idToReturn.ToString());
            return idToReturn;
        }
    }
}