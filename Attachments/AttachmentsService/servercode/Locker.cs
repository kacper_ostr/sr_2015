﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;

namespace AttachmentsService.servercode
{
    /*
     * klasa odpowiedzialna za przetrzymywanie informacji o zablokowaniu jakiegoś resource'a i z w związku z jaką operacją został zablokowany
     */
    public class Locker
    {
        public Locker()
        {
            locker = new Mutex();
            tType = transactionType.NONE;
        }

        public Mutex locker;
        public transactionType tType;
    }
}