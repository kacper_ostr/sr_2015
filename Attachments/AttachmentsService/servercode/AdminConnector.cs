﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace AttachmentsService.servercode
{
    /*
     * Klasa klienta łączącego się do konsoli administratora
     * ponieważ w WebService w .Net nie można wywołać konsoli
     * stąd pomysł na połączenie do zewnętrznego serwera 
     * administrator
     */ 
    public class AdminConnector
    {
        /*
         * funkcja która jako paramter przyjmuje wiadomosc dla administratora
         * i zwraca true lub false w zaleznosci od tego co wybrał administrator
         */ 
        public bool askAdmin(string message)
        {
            try
            {
                /*
                 * standardowa implementacja na podstawie CodeProject
                 */
                TcpClient tcpclnt = new TcpClient();                
                tcpclnt.Connect("127.0.0.1", 9999);                
                Stream stm = tcpclnt.GetStream();
                ASCIIEncoding asen = new ASCIIEncoding();
                byte[] ba = asen.GetBytes(message);                
                stm.Write(ba, 0, ba.Length);
                byte[] bb = new byte[100];
                int k = stm.Read(bb, 0, 100);
                string answer = "";
                for (int i = 0; i < k; i++)
                    answer += Convert.ToChar(bb[i]);
                tcpclnt.Close();
                if (answer == "yes")
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error..... " + e.StackTrace);
                return false;
            }
        }
    }
}