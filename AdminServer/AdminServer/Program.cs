﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;


namespace AdminServer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\nKonsola do kontrolowania transakcji\n");

            try
            {
                IPAddress ipAd = IPAddress.Parse("127.0.0.1");
                TcpListener myList = new TcpListener(ipAd, 9999);
                myList.Start();
                while (true)
                {
                    Socket s = myList.AcceptSocket();
                    byte[] b = new byte[100];
                    int k = s.Receive(b);
                    Console.WriteLine("-------------------------------------");
                    for (int i = 0; i < k; i++)
                        Console.Write(Convert.ToChar(b[i]));
                    Console.WriteLine();
                    Console.WriteLine("yes/no?");
                    Console.WriteLine("");
                    string answer = Console.ReadLine();
                    ASCIIEncoding asen = new ASCIIEncoding();
                    s.Send(asen.GetBytes(answer));
                    s.Close();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error..... " + e.StackTrace);
            }    
        }
    }
}
