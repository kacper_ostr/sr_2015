/*=====================================================================
  File:      AppBase.cs
 
  Summary:   This is a base class used by the various samples for
             various configuration and commandline parsing needs.
 
---------------------------------------------------------------------
  This file is part of the Web Services Enhancements 2.0 for Microsoft .NET Samples.

  Copyright (C) Microsoft Corporation.  All rights reserved.

This source code is intended only as a supplement to Microsoft
Development Tools and/or on-line documentation.  See these other
materials for detailed information regarding Microsoft code samples.

This sample is designed to demonstrate WSE features and is not intended 
for production use. Code and policy for a production application must be 
developed to meet the specific data and security requirements of the 
application.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
PARTICULAR PURPOSE.
=====================================================================*/

using System;
using System.Collections;
using System.Configuration;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Services.Protocols;

using Microsoft.Web.Services2.Security;
using Microsoft.Web.Services2.Security.Tokens;
using Microsoft.Web.Services2.Security.X509;

namespace Microsoft.Web.Services2.QuickStart
{
    /// <summary>
    /// This is a base class used by the various samples for
    /// various configuration and commandline parsing needs.
    /// </summary>
    public class AppBase
    {
        // Replace the key id below with your own certificate Id in Base64String here
        // You can get these values for any cert with the WSE 2.0 Certificate Tool
        public static string ClientBase64KeyId = "gBfo0147lM6cKnTbbMSuMVvmFY4=";
        public static string ServerBase64KeyId = "bBwPfItvKp3b6TNDq+14qs58VJQ=";

        // If users turned on the useRFC3280 flag in the <x509> configuration,
        // they need to use the following certificate Id to verify the incoming certificate
        public static string ClientBase64KeyIdUseRFC3280 = "tyOZ9U6iyFdA0PwC6qgRfePd57s=";
        public static string ServerBase64KeyIdUseRFC3280 = "M9WWSXczVFt2otJ3adPjfmbUxU8=";

        /// <summary>
        /// Looks for any configuration settings that may modify
        /// the Web service proxy used and modifies the proxy accordingly.
        /// </summary>
        protected void ConfigureProxy(HttpWebClientProtocol protocol)
        {
            string remoteHost = ConfigurationSettings.AppSettings["remoteHost"];
            if (remoteHost != null)
            {
                Uri remoteHostUri = new Uri(remoteHost);
                Uri protocolUrl = new Uri(protocol.Url);
                Uri newUri = new Uri(remoteHostUri, protocolUrl.AbsolutePath);

                if (protocol is Microsoft.Web.Services2.WebServicesClientProtocol) 
                {
                    ((Microsoft.Web.Services2.WebServicesClientProtocol)protocol).Url = newUri.AbsoluteUri;
                }
                else
                {
                    protocol.Url = newUri.AbsoluteUri;
                }
            }
        }
    
        protected static void Error(Exception e) 
        {
            Console.WriteLine("****** Exception Raised ******");
            StringBuilder sb = new StringBuilder();
            ProcessException(e, sb);
            Console.WriteLine(sb.ToString());
            Console.WriteLine("******************************");
        }

        private static void ProcessException(Exception e, StringBuilder sb)
        {
            if (e != null)
            {
                if (e is WebException)
                {
                    // Process the Web Exception.
                    WebException webExcep = e as WebException;
                    if (webExcep.Response != null)
                    {
                        WebResponse response = webExcep.Response;
                        string str = webExcep.Message;
                        Stream responseStream = response.GetResponseStream();

                        if ( responseStream.CanRead ) 
                        {
                            StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                            string excepStr = reader.ReadToEnd();
                            sb.Append("Web Exception Occured: " + excepStr);
                        }
                        else 
                        {
                            sb.Append("Web Exception Occured: " + e.ToString());
                        }
                    }
                    else
                    {
                        sb.Append("Web Exception Occured: " + e.ToString());
                    }
                }
                else 
                {
                    if (e is System.Web.Services.Protocols.SoapException)
                    {
                        System.Web.Services.Protocols.SoapException se = e as System.Web.Services.Protocols.SoapException;
                        sb.Append("System.Web.Services.Protocols.SoapException:");
                        sb.Append(Environment.NewLine);
                        sb.Append("SOAP-Fault code: " + se.Code.ToString());
                        sb.Append(Environment.NewLine);
                        sb.Append("Message: ");
                    }
                    else
                    {
                        sb.Append(e.GetType().FullName);
                        sb.Append(": ");
                    }

                    sb.Append(e.Message);

                    if (e.InnerException != null)
                    {
                        sb.Append(" ---> ");
                        ProcessException(e.InnerException, sb);
                        sb.Append(Environment.NewLine);
                        sb.Append("--- End of Inner Exception ---");
                    }

                    if (e.StackTrace != null)
                    {
                        sb.Append(Environment.NewLine);
                        sb.Append(e.StackTrace);
                    }
                }
            }
        }

        /// <summary>
        /// Routine to compare two byte arrays
        /// </summary>
        /// <param name="a">First Byte Array</param>
        /// <param name="b">Second Byte Array</param>
        /// <returns>true or false</returns>
        public static bool CompareArray(byte[] a, byte[] b)
        {
            if (a != null && b != null && a.Length == b.Length)
            {
                int index = a.Length;
                while (--index > -1)
                    if (a[index] != b[index])
                        return false;
                return true;
            }
            else if (a == null && b == null)
                return true;
            else
                return false;
        }

        /// <summary>
        /// This method basically checks if those required message parts exist.
        /// The required headers are Soap:body, To, Action, MessageID, and Timestamp
        /// </summary>
        /// <param name="context"></param>
        public static void VerifyMessageParts(SoapContext context) 
        {
            // Body
            if ( context.Envelope.Body == null )
                throw new ApplicationException("The message must contain a soap:Body element");

            if ( context.Addressing.To == null || context.Addressing.To.TargetElement == null )
                throw new ApplicationException("The message must contain a wsa:To header");

            if ( context.Addressing.Action == null || context.Addressing.Action.TargetElement == null )
                throw new ApplicationException("The message must contain a wsa:Action header");

            if ( context.Addressing.MessageID == null || context.Addressing.MessageID.TargetElement == null )
                throw new ApplicationException("The message must contain a wsa:To header");

            if ( context.Security.Timestamp == null  )
                throw new ApplicationException("The message must contain a wsu:Timestamp header");
        }

        /// <summary>
        /// This method checks if the incoming message has signed all the important message parts
        /// such as soap:Body, all the addressing headers and timestamp.  
        /// </summary>
        /// <param name="context"></param>
        /// <returns>The signing token</returns>
        public static SecurityToken GetSigningToken(SoapContext context)
        {
            foreach ( ISecurityElement element in context.Security.Elements )
            {
                if ( element is MessageSignature )
                {
                    // The given context contains a Signature element.
                    MessageSignature sig = element as MessageSignature;

                    if (CheckSignature(context, sig))
                    {
                        // The SOAP Body is signed.
                        return sig.SigningToken;
                    }
                }
            }

            return null;
        }

        static bool CheckSignature(SoapContext context, MessageSignature signature)
        {
            //
            // Now verify which parts of the message were actually signed.
            //
            SignatureOptions actualOptions   = signature.SignatureOptions;
            SignatureOptions expectedOptions = SignatureOptions.IncludeSoapBody;
          
            if (context.Security != null && context.Security.Timestamp != null ) 
                expectedOptions |= SignatureOptions.IncludeTimestamp;
            
            //
            // The <Action> and <To> are required addressing elements.
            //
            expectedOptions |= SignatureOptions.IncludeAction;
            expectedOptions |= SignatureOptions.IncludeTo;

            if ( context.Addressing.FaultTo != null && context.Addressing.FaultTo.TargetElement != null )
                expectedOptions |= SignatureOptions.IncludeFaultTo;

            if ( context.Addressing.From != null && context.Addressing.From.TargetElement != null )
                expectedOptions |= SignatureOptions.IncludeFrom;

            if ( context.Addressing.MessageID != null && context.Addressing.MessageID.TargetElement != null )
                expectedOptions |= SignatureOptions.IncludeMessageId;

            if ( context.Addressing.RelatesTo != null && context.Addressing.RelatesTo.TargetElement != null )
                expectedOptions |= SignatureOptions.IncludeRelatesTo;

            if ( context.Addressing.ReplyTo != null && context.Addressing.ReplyTo.TargetElement != null )
                expectedOptions |= SignatureOptions.IncludeReplyTo;
            //
            // Check if the all the expected options are the present.
            //
            return ( ( expectedOptions & actualOptions ) == expectedOptions );
                
        }

        /// <summary>
        /// This method checks if the incoming message has encrypted the soap message body 
        /// </summary>
        /// <param name="context">the soap context to search for</param>
        /// <returns>The encrypting token</returns>
        public static SecurityToken GetEncryptingToken(SoapContext context)
        {
            SecurityToken encryptingToken = null;

            foreach (ISecurityElement element in context.Security.Elements)
            {
                if (element is EncryptedData)
                {
                    EncryptedData encryptedData = element as EncryptedData;
                    System.Xml.XmlElement targetElement = encryptedData.TargetElement;										
							
                    if ( SoapEnvelope.IsSoapBody(targetElement))
                    {
                        // The given context has the Body element Encrypted.
                        encryptingToken = encryptedData.SecurityToken;
                    }
                }
            }

            return encryptingToken;
        }

        /// <summary>
        /// Return the client certificate usually used for signing the message
        /// </summary>
        /// <param name="selectFromList">a flag to decide if we need to use the X.509 dialog</param>
        /// <returns>X509Security found for client</returns>
        public static X509SecurityToken GetClientToken(bool selectFromList)
        {
            X509SecurityToken token = null;

            // Open the CurrentUser Certificate Store and try MyStore only
            X509CertificateStore store = X509CertificateStore.CurrentUserStore( X509CertificateStore.MyStore );
            
            if ( selectFromList )
            {
                token = RetrieveTokenFromDialog(store);
            }
            else 
            {
                token = RetrieveTokenFromStore(store, ClientBase64KeyId);
            }

            return token;
        }

        /// <summary>
        /// Return the server certificate usually used for encrypting the message.
        /// </summary>
        /// <param name="selectFromList">a flag to decide if we need to use the X.509 dialog</param>
        /// <returns>X509Security found for server</returns>
        public static X509SecurityToken GetServerToken(bool selectFromList, bool isClient)
        {
            X509SecurityToken token = null;
            X509CertificateStore store = null;

            if (isClient)
            {
                // Open the CurrentUser Certificate Store and try OtherPeople first
                store = X509CertificateStore.CurrentUserStore( X509CertificateStore.OtherPeople );
            }
            else
            {
                // For server, open the LocalMachine Certificate Store and try Personal store.
                store = X509CertificateStore.LocalMachineStore( X509CertificateStore.MyStore );
            }

            if ( selectFromList )
            {
               token = RetrieveTokenFromDialog(store);

                if ( token == null )
                {
                    if (isClient)
                    {
                        store = X509CertificateStore.CurrentUserStore( X509CertificateStore.MyStore );
                        token = RetrieveTokenFromDialog(store);
                    }
                    else
                    {
                        // For server the server Certificate should be installed in the Personal 
                        // store. We tried that store and either we were unable to open the store
                        // or the certificate does not exist. So we don't have a fallback for this
                        // case.
                    }
                }
            }
            else 
            {
                token = RetrieveTokenFromStore(store, ServerBase64KeyId);
           
                //
                // If we failed to retrieve it from the OtherPeople,
                // we now try the MyStore
                //
                if ( token == null )
                {
                    if (isClient)
                    {
                        store = X509CertificateStore.CurrentUserStore( X509CertificateStore.MyStore );
                        token = RetrieveTokenFromStore(store, ServerBase64KeyId);
                    }
                    else
                    {
                        // For server the server Certificate should be installed in the Personal 
                        // store. We tried that store and either we were unable to open the store
                        // or the certificate does not exist. So we don't have a fallback for this
                        // case.
                    }
                }
            }

            return token;
        }

        private static X509SecurityToken RetrieveTokenFromDialog(X509CertificateStore store) 
        {
            if ( store == null )
                throw new ArgumentNullException("store");
            
            X509SecurityToken token = null;

            try 
            {
                if( store.OpenRead() ) 
                {
                    // Use the dialog in sampleBase to prompt user
                    StoreDialog dialog = new StoreDialog( store );
                    X509Certificate cert = dialog.SelectCertificate( IntPtr.Zero, "Select Certificate", "Choose a Certificate below." );
                    token = new X509SecurityToken( cert );
                }
            }
            finally 
            {
                if ( store != null )
                    store.Close();
            }

            return token;
        }

        private static X509SecurityToken RetrieveTokenFromStore(X509CertificateStore store, string keyIdentifier) 
        {
            if ( store == null )
                throw new ArgumentNullException("store");
            
            X509SecurityToken token = null;

            try 
            {
                if( store.OpenRead() ) 
                {
                    // Place the key ID of the certificate in a byte array
                    // This KeyID represents the Wse2Quickstart certificate included with the WSE 2.0 Quickstarts
                    // ClientBase64KeyId is defined in the ClientBase.AppBase class
                    X509CertificateCollection certs = store.FindCertificateByKeyIdentifier( Convert.FromBase64String( keyIdentifier ) );

                    if (certs.Count > 0)
                    {
                        // Get the first certificate in the collection
                        token = new X509SecurityToken( ((X509Certificate) certs[0]) );
                    }        
                } 
            }
            finally
            {
                if ( store != null )
                    store.Close();
            }
  
            return token;
        }
    }
}

 