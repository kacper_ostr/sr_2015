/*=====================================================================
  File:      ServiceTokenCollection.cs
 
  Summary:   This is a configuration handler class which loads the custom token
             issuer section.
 
---------------------------------------------------------------------
  This file is part of the Web Services Enhancements 2.0 for Microsoft .NET Samples.

  Copyright (C) Microsoft Corporation.  All rights reserved.

This source code is intended only as a supplement to Microsoft
Development Tools and/or on-line documentation.  See these other
materials for detailed information regarding Microsoft code samples.

This sample is designed to demonstrate WSE features and is not intended 
for production use. Code and policy for a production application must be 
developed to meet the specific data and security requirements of the 
application.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
PARTICULAR PURPOSE.
=====================================================================*/

namespace Microsoft.Web.Services2.QuickStart
{
    using System;
    using System.Collections;

    using Microsoft.Web.Services2.Policy;
    using Microsoft.Web.Services2.Security;
    using Microsoft.Web.Services2.Security.Tokens;

    /// <summary>
    /// This class represent a list of security tokens with key type of Uri. 
    /// </summary>
    public class ServiceTokenCollection
    {            
        private Hashtable _serviceTokens = new Hashtable();  
         
        /// <summary>
        /// Constructor
        /// </summary>
        public ServiceTokenCollection() 
        {
        }
        
        /// <summary>
        /// Given a uri, this method will iterate through its keys collection 
        /// searching for the longest prefix match.  Once the entry is found,
        /// it will return the matched prefix as well as the security token 
        /// corresponding to that mached prefix.  Return null for no match. 
        /// </summary>
        public SecurityToken this[Uri uri] 
        {
            get 
            {
                if ( uri == null )
                    throw new ArgumentNullException("uri");

                Uri matchedPrefix = null;
                
                foreach( Uri prefix in _serviceTokens.Keys) 
                {
                    if ( PrefixEquals(uri, prefix) ) 
                    {
                        //
                        // We will choose the longest prefix match
                        //
                        if ( matchedPrefix == null                       ||
                            PrefixEquals(prefix, matchedPrefix))  
                        {
                            matchedPrefix = prefix;  // remember the prefix for perf reason
                        }
                    }
                }

                //
                // return the token found or null if no match
                //
                return _serviceTokens[matchedPrefix] as SecurityToken;
            }
        }
 
        /// <summary>
        /// Add a security token to the collection.
        /// It will throw argumentException if the token added does not 
        /// have an AppliesTo.
        /// </summary>
        public void Add(Uri appliesTo, SecurityToken serviceToken)
        {
            if ( serviceToken == null )
                throw new ArgumentNullException("serviceToken");

            if ( appliesTo == null )
                throw new ArgumentException("appliesTo");

            _serviceTokens.Add(appliesTo, serviceToken);
        }        

        /// <summary>
        /// Get the total number of entries in the collection
        /// </summary>
        public int Count 
        {
            get 
            {
                return _serviceTokens.Count;
            }
        }

        /// <summary>
        /// Return the enumerator
        /// </summary>
        public IEnumerator GetEnumerator()
        {
            return _serviceTokens.Values.GetEnumerator();
        }

        /// <summary>
        /// Return true if this collection is synchronized
        /// </summary>
        public bool IsSynchronized
        {
            get
            {
                return _serviceTokens.IsSynchronized;
            }
        }

        /// <summary>
        /// Given a security token request, this method will return a security token with
        /// the longest prefix match.  This method will throw Client fault exception if there
        /// is no match found and the given appliesTo is not null.  It also throws Client fault if
        /// there is more than one service tokens registered but there is no appliesTo in
        /// the ReqeustSecurityToken.  Note here we try to match the WSAddessing:To header against
        /// given appliesTo if there is no service tokens registered in the configuration file.
        /// </summary>
        public SecurityToken RetrieveServiceToken(RequestSecurityToken request)
        {
            if ( request == null )
                throw new ArgumentNullException("request");

            AppliesTo appliesToInRequest = request.AppliesTo;
            SecurityToken serviceToken = null;

            if ( appliesToInRequest != null )
            {
                //
                // ApplesTo exists case:
                // We try to find the longest prefix match from the configuration file,
                // so we can add <keys> element in the SCT
                //
                Uri appliesTo = appliesToInRequest.EndpointReference.Address.Value;

                if ( Count == 0 )
                {    
                    //
                    // The token issuer has zero service token registered
                    //
                    throw new ApplicationException("Must register a service token");
                }
                else
                {
                    //
                    // The token issuer has one or more service tokens registered
                    //
                    serviceToken = this[appliesTo];

                    if ( serviceToken == null )
                        throw new ApplicationException("No service token was found in the configuration file which matches the following appliesTo" + appliesTo.ToString());
                }
            }
            else
            {
                //
                // No appliesTo case:
                // Throws if there is one or more service token registered in the configuration file
                //
                if (  Count > 0 )
                    throw new ApplicationException("Must have an appliesTo in the RequestSecurityToken message");
            }

            return serviceToken;
        }

        private bool HasNoQuery(Uri uri)
        {
            return (uri.Query.Length == 0 );
        }

        private string Append(Uri uri1, Uri uri2) 
        {
            if ( HasNoQuery (uri1)) 
            {
                if(HasNoQuery(uri2)) 
                    return uri1.AbsolutePath.EndsWith("/") ? uri1.AbsolutePath : (uri1.AbsolutePath + '/');
                else  // if the other uri has fragment or query, don't add trailing slash!!!
                    return uri1.AbsolutePath;
            }
            else //  ( uri1.Query != string.Empty )
                return uri1.AbsolutePath + uri1.Query;
        }

        bool PrefixEquals(Uri fullUri, Uri prefix) 
        {
            return (prefix.Scheme == fullUri.Scheme)
                && (prefix.Host == fullUri.Host)
                && (prefix.Port == fullUri.Port)
                && Append(fullUri, prefix).StartsWith(Append(prefix, fullUri));
        }

        /// <summary>
        /// Return the sync root
        /// </summary>
        public object SyncRoot
        {
            get
            {
                return _serviceTokens.SyncRoot;
            }
        }
    }
}
 