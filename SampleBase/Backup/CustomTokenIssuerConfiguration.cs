
/*=====================================================================
  File:      CustomTokenIssuerConfiguration.cs
 
  Summary:   This is a configuration handler class which loads the custom token
             issuer section.
 
---------------------------------------------------------------------
  This file is part of the Web Services Enhancements 2.0 for Microsoft .NET Samples.

  Copyright (C) Microsoft Corporation.  All rights reserved.

This source code is intended only as a supplement to Microsoft
Development Tools and/or on-line documentation.  See these other
materials for detailed information regarding Microsoft code samples.

This sample is designed to demonstrate WSE features and is not intended 
for production use. Code and policy for a production application must be 
developed to meet the specific data and security requirements of the 
application.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
PARTICULAR PURPOSE.
=====================================================================*/

namespace Microsoft.Web.Services2.QuickStart
{
    using System;
    using System.Collections;
    using System.Configuration;
    using System.Security.Cryptography.Xml;
    using System.Xml;

    using Microsoft.Web.Services2.Security;
    using Microsoft.Web.Services2.Security.Tokens;


	/// <summary>
	/// CustomTokenIssuerConfiguration
	/// </summary>
	public class CustomTokenIssuerConfiguration : IConfigurationSectionHandler
	{
        static CustomTokenIssuerConfiguration   _configuration          = null;
        static object                           _configurationLock      = new object();
        static ServiceTokenCollection           _serviceTokens          = new ServiceTokenCollection();
        static Hashtable                        _serviceTokenKeyInfos   = new Hashtable();

		public CustomTokenIssuerConfiguration()
		{	
		}

        static void Initialize()   
        {
            if ( _configuration == null )
            {
                lock ( _configurationLock )
                {
                    if ( _configuration == null )
                    {
                        _configuration = ConfigurationSettings.GetConfig("customTokenIssuer") as CustomTokenIssuerConfiguration;
                    }
                }
            }
        }

        object IConfigurationSectionHandler.Create(Object parent, object configContext, XmlNode section)
        {
            try 
            {
                if ( section.HasChildNodes )
                {
                    foreach ( XmlNode node in section.ChildNodes )
                    {
                        XmlElement element = node as XmlElement;

                        if ( element != null ) 
                        {
                            switch ( element.LocalName )
                            {
                                case "serviceToken":
                                    //
                                    // URI attribute
                                    //
                                    string uri = element.GetAttribute("URI");

                                    if ( _serviceTokenKeyInfos.Contains(uri) )
                                        throw new ConfigurationException("There are more then one service token entries configured for the following URI: " + uri);

                                    //
                                    // No other attributes on this child !
                                    //
                                    foreach ( XmlNode serviceNode in element.ChildNodes )
                                    {
                                        if ( serviceNode is XmlElement )
                                        {
                                            XmlElement serviceChild = serviceNode as XmlElement;

                                            switch ( serviceChild.NamespaceURI )
                                            {
                                                case XmlSignature.NamespaceURI:
                                                switch ( serviceChild.LocalName )
                                                {
                                                    case XmlSignature.ElementNames.KeyInfo:
                                                        //
                                                        // Load those service tokens keyinfos from the web.config
                                                        //
                                                        KeyInfo serviceKeyInfo = KeyInfoHelper.LoadXmlKeyInfo(serviceChild);

                                                        System.Diagnostics.Debug.Assert(serviceKeyInfo!=null);
                                                        _serviceTokenKeyInfos.Add(uri, serviceKeyInfo);
                                                        break;
                                                    default:
                                                        
                                                        break;
                                                }
                                                    break;

                                                default:
                                                    break;
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            finally 
            {
                _configuration = this;
            }

            return this;
        }

        /// <devdoc>
        /// get a list of ServiceTokens registered in the tokenIssuer configuration section
        /// The key is uri
        /// </devdoc>
        public static ServiceTokenCollection ServiceTokens
        {
            get
            {
                Initialize();

                if ( _serviceTokens.Count == 0 && _serviceTokenKeyInfos.Count > 0)
                {
                    lock ( _configurationLock )
                    {
                        if ( _serviceTokens.Count == 0 )
                        {
                            foreach ( string uri in _serviceTokenKeyInfos.Keys )
                            {
                                SecurityToken token = SecurityTokenManager.GetTokenFromKeyInfo(_serviceTokenKeyInfos[uri] as KeyInfo);

                                //
                                // Throw exception if there is a keyinfo but tokenProvider fails to return a securityToken based on that keyinfo
                                //
                                if ( null == token )
                                {
                                    throw new ConfigurationException("Failed to resolve the service key info registered for URI: " + uri);
                                }

                                //
                                // Set the service token's applies to be the registered uri
                                //
                                _serviceTokens.Add(new Uri(uri), token);
                            }
                        }
                    }
                }

                return _serviceTokens;
            }
        }
	}
}
 