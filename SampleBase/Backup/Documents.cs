/*=====================================================================
  File:      Documents.cs
 
  Summary:   This files contains XML Serializable classes which are used
             to send and receive messages.
 
---------------------------------------------------------------------
  This file is part of the Web Services Enhancements 2.0 for Microsoft .NET Samples.

  Copyright (C) Microsoft Corporation.  All rights reserved.

This source code is intended only as a supplement to Microsoft
Development Tools and/or on-line documentation.  See these other
materials for detailed information regarding Microsoft code samples.

This sample is designed to demonstrate WSE features and is not intended 
for production use. Code and policy for a production application must be 
developed to meet the specific data and security requirements of the 
application.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
PARTICULAR PURPOSE.
=====================================================================*/

using System;
using System.Xml.Serialization;

namespace Microsoft.Web.Services2.QuickStart
{

    //
    // This class is an XML Serializable class for a StockQuote complexType
    //
    //  <StockQuote xmlns="http://stockservice.contoso.com/wse/samples/2003/06">
    //      <Symbol>string</Symbol>
    //      <Last>double</Last>
    //      <Date>dateTime</Date>
    //      <Change>double</Change>
    //      <Open>double</Open>
    //      <High>double</High>
    //      <Low>double</Low>
    //      <Volume>long</Volume>
    //      <MarketCap>long</MarketCap>
    //      <PreviousClose>double</PreviousClose>
    //      <PreviousChange>double</PreviousChange>
    //      <Low52Week>double</Low52Week>
    //      <High52Week>double</High52Week>
    //      <Name>string</Name>
    //   </StockQuote>    
    //
    [XmlRoot(Namespace="http://stockservice.contoso.com/wse/samples/2003/06")]
    public class StockQuote
    {
        public String   Symbol;
        public double   Last;
        public DateTime Date;
        public double   Change;
        public double   Open;
        public double   High;
        public double   Low;
        public long     Volume;
        public long     MarketCap;
        public double   PreviousClose;
        public double   PreviousChange;
        public double   Low52Week;
        public double   High52Week;
        public String   Name;
    }

    //
    // This class is an XML Serializable class for a StockSynbols complexType. Note: When this 
    // class is used within the StockQuoteRequest class, it will serialize differently
    // than when it is serialized in a stand-alone fashion.
    //
    // <symbols xmlns="http://stockservice.contoso.com/wse/samples/2003/06">
    //      <Symbol>string</Symbol>
    //      <Symbol>string</Symbol>
    // </symbols>
    //
    [XmlRoot(Namespace="http://stockservice.contoso.com/wse/samples/2003/06")]
    public class StockSymbols
    {
        [XmlElement("Symbol")]          
        public String[] Symbols;                       
    }

    //
    // This class is an XML Serializable class for a StockQuoteRequest complexType
    //
    // <StockQuoteRequest xmlns="http://stockservice.contoso.com/wse/samples/2003/06">
    //      <symbols>
    //          <Symbol>string</Symbol>
    //          <Symbol>string</Symbol>
    //      </symbols>
    // </StockQuoteRequest>
    //
    [XmlRoot(Namespace="http://stockservice.contoso.com/wse/samples/2003/06")]
    public class StockQuoteRequest
    {             
        [XmlArray("symbols")]
        [XmlArrayItem("Symbol")]
        public String[] Symbols;                       
    }

    //
    // This class is an XML Serializable class for a StockQuotes complexType
    //
    // <StockQuotes xmlns="http://stockservice.contoso.com/wse/samples/2003/06">
    //  <StockQuote>
    //      <Symbol>string</Symbol>
    //      <Last>double</Last>
    //      <Date>dateTime</Date>
    //      <Change>double</Change>
    //      <Open>double</Open>
    //      <High>double</High>
    //      <Low>double</Low>
    //      <Volume>long</Volume>
    //      <MarketCap>long</MarketCap>
    //      <PreviousClose>double</PreviousClose>
    //      <PreviousChange>double</PreviousChange>
    //      <Low52Week>double</Low52Week>
    //      <High52Week>double</High52Week>
    //      <Name>string</Name>
    //   </StockQuote>
    //  <StockQuote>
    // </StockQuotes>
    //
    public class StockQuotes
    {
        [XmlElement("StockQuote")]
        public StockQuote[] Quotes;    
    }

}
 